<p align="center">
  <a href="https://www.intechinfo.fr/" target="blank"><img src="https://axeptio.imgix.net/2022/08/bfdc5e46-94c4-426f-8b5d-47ff8f1c46c7.png?auto=format&fit=crop&w=170&h=auto&dpr=2" width="120" alt="Intech Logo" /></a>
</p>

# cicd-2023

## Description

Intech CI-CD 

## Architecture

![](/assets/intech-cicd.drawio.png)


## Getting started

- [ ] [Configure your access to gitlab using ssh](https://docs.gitlab.com/ee/user/ssh.html)
- [ ] [Create a new project repository in gitlab](https://mssg.ipta.demokritos.gr/gitlab/help/gitlab-basics/create-project.md)
- [ ] [Clone the repository on your local machine](https://docs.gitlab.com/ee/user/project/repository/)


## Create your first backend service

- [ ] [Create your first backend service using NestJS (or similar)](https://docs.nestjs.com/first-steps)
- [ ] [Create a healthcheck endpoint](https://docs.nestjs.com/first-steps)
- [ ] [Add a simple test to your healthcheck using Jest](https://jestjs.io/docs/getting-started)


## Create your first pipeline 


- [ ] [Add a simple pipeline to your project with 2 stages](https://docs.gitlab.com/ee/ci/quick_start)
    - [build] 
        - [build] 
    - [test]
        - [test]
        
- [ ] [Add a seccond step to the build stage to lint the code](https://eslint.org/)
    - [build] 
        - [build] 
        - [lint] 
    - [test]
        - [test]

- [ ] [Add code coverage to your code using cobertura](https://gist.github.com/rishitells/3c4536131819cff4eba2c8ab5bbb4570)
    - [build] 
        - [build] 
        - [lint] 
    - [test]
        - [test]
    - [coverage]
        - [pages]

- [ ] [Add Badges to your project to display pipeline status and code coverage](https://docs.gitlab.com/ee/user/project/badges.html)



## Publish a Docker Image to Gitlab Registry

- [ ] [Create a Dockerfile for your project](https://dev.to/erezhod/setting-up-a-nestjs-project-with-docker-for-back-end-development-30lg)
- [ ] [Add a publish stage to your pipeline](https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html)

    - [build] 
        - [build] 
        - [lint] 
    - [test]
        - [test]
    - [coverage]
        - [pages]
    - [publish]
        - [publish]
        - [publish_to_gcp]

## Publish a Docker Image to GCP Artifact Registry

- [ ] [Add a publish publish_to_gcp to your pipeline](https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html)

    - [build] 
        - [build] 
        - [lint] 
    - [test]
        - [test]
    - [coverage]
        - [pages]
    - [publish]
        - [publish]
        - [publish_to_gcp]


## Deploy your image on cloud run

- [ ] [Create a new Cloud Run instance with your image](https://cloud.google.com/run/docs)


## Feature Flag

- [ ] [Create a feature flag in gitlab](https://docs.gitlab.com/ee/user/ssh.html)
- [ ] [Use the Feature Flag in your Service](https://docs.getunleash.io/reference/sdks/node)



## Create a TS library

- [ ] [Create a new project repository in gitlab](https://mssg.ipta.demokritos.gr/gitlab/help/gitlab-basics/create-project.md)
- [ ] [Clone the repository on your local machine](https://docs.gitlab.com/ee/user/project/repository/)
- [ ] [Create your first TS library](https://aganglada.com/blog/how-to-create-your-own-typescript-library)
- [ ] [Create a pipeline for your library]
    - [build] 
        - [build] 
        - [lint] 
    - [test]
        - [test]
    - [coverage]
        - [pages]


- [ ] [Add a stage to publish the library in gitlab registry](https://docs.gitlab.com/ee/user/packages/npm_registry/)

    - [build] 
        - [build] 
        - [lint] 
    - [test]
        - [test]
    - [coverage]
        - [pages]
    - [publish]
        - [publish]



## Deploy a Front-End tro firebase hosting
- [ ] [Create a new project repository in gitlab](https://mssg.ipta.demokritos.gr/gitlab/help/gitlab-basics/create-project.md)
- [ ] [Clone the repository on your local machine](https://docs.gitlab.com/ee/user/project/repository/)
- [ ] [Create a Single Page Application using React](https://create-react-app.dev/)
- [ ] [Create a pipeline with a deploy stage](https://about.gitlab.com/blog/2020/03/16/gitlab-ci-cd-with-firebase/)

    - [build] 
        - [build] 
    - [deploy]
        - [deploy]


